package com.payroll.businessLogic;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Time Utilities
 */
public class TimeUtils {
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
    private static final SimpleDateFormat YMD_DATE_FORMAT = new SimpleDateFormat("yyyyMMdd");

    /**
     * Build a date object from provided date string.
     *
     * @param dateString A string in date format patter "yyyy/MM/dd HH:mm:ss"
     * @return A <code>java.util.Date</code> object represent the date string.
     */
    public static Date buildDate(String dateString) {
        try {
            return DATE_FORMAT.parse(dateString);
        } catch (ParseException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Create a date object from year, month, and day-of-month.
     * Note that in caledar.set(time), month starts with 0.
     *
     * @param year  value of expected year
     * @param month value of expected month
     * @param mday  value of expected day of month
     * @return A <code>java.util.Date</code> object as specified parameters
     */
    public static Date buildDate(int year, int month, int mday) {
        Calendar c = Calendar.getInstance();
        c.set(year, month - 1, mday);
        return c.getTime();
    }

    /**
     * Return true iff date2 is greater than or equals to date1.
     *
     * @param date1 Date object
     * @param date2 Date object
     * @return boolean value iff date2 is greater than or equals to date1
     */
    public static boolean isDateEarlierThan(Date date1, Date date2) {
        return date2.getTime() >= date1.getTime();
    }

    /**
     * Compare the date objects provided, (date2 - date1), return value in millisecond.
     *
     * @param date1 Date object
     * @param date2 Date object
     * @return value in millisecond
     */
    public static long compareDate(Date date1, Date date2) {
        return date2.getTime() - date1.getTime();
    }

    /**
     * Return true iff date is before or equals to hour (24H clock)
     *
     * @param date Date object
     * @param hour Hour, 24H representation
     * @return Return true iff date is before or equals to hour (24H clock)
     */
    public static boolean isTimeBefore(Date date, int hour) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return date.getTime() <= c.getTimeInMillis();
    }

    /**
     * Return true iff date is after or equals to hour (24H clock)
     *
     * @param date Date object
     * @param hour Hour, 24H representation
     * @return Return true iff date is after or equals to hour (24H clock)
     */
    public static boolean isTimeAfter(Date date, int hour) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.set(Calendar.HOUR_OF_DAY, hour);
        c.set(Calendar.MINUTE, 0);
        c.set(Calendar.SECOND, 0);
        c.set(Calendar.MILLISECOND, 0);
        return date.getTime() >= c.getTimeInMillis();
    }

    /**
     * Return true iff date is on weekend (Saturday or Sunday)
     *
     * @param date Date object
     * @return Return true iff date is on weekend (Saturday or Sunday)
     */
    public static boolean isWeekend(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        int w = c.get(Calendar.DAY_OF_WEEK);
        return Calendar.SATURDAY == w || Calendar.SUNDAY == w;
    }

    /**
     * Check if two dates are in the same day period
     *
     * @param date1 first date
     * @param date2 second date
     * @return true if these two dates are in the same day period
     */
    public static boolean isSameDay(Date date1, Date date2) {
        if (date1 == null || date2 == null) return false;
        return YMD_DATE_FORMAT.format(date1).equals(YMD_DATE_FORMAT.format(date2));
    }
}
